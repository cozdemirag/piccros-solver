﻿using Services;

namespace PiccrosSolver
{
    using System.Linq;
    using System;
    using Models;
    using Newtonsoft.Json;

    class Program
    {
        private static LineInfoTextParsing parser = new LineInfoTextParsing();
        private static MissingBlockFinder finder = new MissingBlockFinder();
        private static BoardJsonParser boardParser = new BoardJsonParser();

        static void Main(string[] args)
        {
            //var boardInfo = new BoardInfo()
            //{
            //    Horizontal = new BoardSideInfo
            //    {
            //        Length = 2,
            //        Rules = new int[2][]
            //        {
            //            new int[2]{1,2 },new int[2]{2,2}
            //        }
            //    }
            //};

            //var contents = JsonConvert.SerializeObject(boardInfo);


            if (args?[0] == "-board")
            {
                BoardInfoParse(args);
            }
            else if (args?[0] == "-line")
            {
                LineInfoParse(args);
            }
        }

        private static void LineInfoParse(string[] args)
        {
            var lineInfo = parser.Parse(args);

            var solvedLine = finder.FindBlocks(lineInfo);



            var numbers = Enumerable.Range(1, solvedLine.Length).Select(it => it.ToString("D2")).ToArray();
            var result = solvedLine.Select(it => it.Symbol).ToArray();

            var resultString = numbers.Zip(result, (it1, it2) => $"{it1}: {it2}").ToArray();

            Console.WriteLine("Solved!");
            foreach (var entry in resultString)
            {
                Console.WriteLine(entry);
            }
        }

        private static void BoardInfoParse(string[] args)
        {
            var boardInfo = boardParser.Parse(args?[1]);
        }
    }
}