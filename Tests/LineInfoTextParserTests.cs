﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests
{
    [TestClass]
    public class LineInfoTextParserTests
    {
        private LineInfoTextParsing _sut;


        [TestInitialize]
        public void SetUp()
        {
            this._sut = new LineInfoTextParsing();
        }

        [TestMethod]
        public void WhenAllValidDataIsGivenForALineInfo()
        {
            // Arrange
            var expectedSize = 15;
            var expectedRule = new int[] { 1, 1, 1, 1, 1 };
            var expectedRuleSum = 5;
            var expectedMinCrossSum = 4;
            var expectedExistingBlocks = Enumerable.Range(0, expectedSize).Select(it => Block.UNKNOWN).ToArray();

            var parameters = new string[] { "-line", "15", "1,1,1,1,1" };

            // Act
            var actual = this._sut.Parse(parameters);

            // Assert
            MSTestExtensions.IsNotNull("LineInfo Obj", actual);
            MSTestExtensions.AreEqual("Line Length", expectedSize, actual.Length);
            MSTestExtensions.AreEqual("Line Rule Sum", expectedRuleSum, actual.RuleSum);
            MSTestExtensions.AreEqual("Line Min Cross Sum", expectedMinCrossSum, actual.MinRequredCrossedSum);
            MSTestExtensions.AreEqual("Line Existing Block Size", expectedSize, actual.Blocks.Length);
            CollectionAssert.AreEquivalent(expectedRule, actual.Rules);
            CollectionAssert.AllItemsAreNotNull(actual.Blocks);
            CollectionAssert.AreEquivalent(expectedExistingBlocks, actual.Blocks);

        }
    }
}
