﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests
{
    public static class MSTestExtensions
    {
        public static void IsNotNull(string label, object value)
        {
            Assert.IsNotNull(value, $"{label} : Expected Not NULL but found {value}");
        }

        public static void AreEqual<T>(string label, T expected, T actual)
        {
            Assert.AreEqual(expected, actual, $"{label} : Expected {expected} but found {actual}.");
        }

        public static void IsTrue(string label, bool condition)
        {
            Assert.IsTrue(condition, $"{label} : Expected True but found {condition}");
        }
    }
}
