using System;
using System.Linq;
using Models;
using Interfaces;
using Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class MissingBlockFinderTests
    {
        private IMissingBlockFinder _sut;

        [TestInitialize]
        public void Setup()
        {
            this._sut = new MissingBlockFinder();
        }

        [TestMethod]
        public void MinCrossedSumAndRuleSumEqualsToLineLength()
        {
            var lineInfo = new LineInfo(new int[] { 1, 1 }, 1, 2, 3);

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "o", "x", "o");
        }

        [TestMethod]
        public void MinCrossedSumAndRuleSumEqualsToLineLength2()
        {
            var lineInfo = new LineInfo(new int[] { 1, 2, 1 }, 2, 4, 6);

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "o", "x", "o", "o", "x", "o");
        }

        [TestMethod]
        public void MinCrossedSumAndRuleSumEqualsToLineLength3()
        {
            var lineInfo = new LineInfo(new int[] { 1, 1, 1, 1, 1, 1 }, 5, 6, 11, Enumerable.Range(0, 11).Select(it => Block.UNKNOWN).ToArray());

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "o", "x", "o", "x", "o", "x", "o", "x", "o", "x", "o");
        }


        [TestMethod]
        public void LengthGreaterThanRuleSumAndMinCrossedSum_CommonResults()
        {
            var lineInfo = new LineInfo(new int[] { 2, 1 }, 1, 3, 5, Enumerable.Range(0,5).Select(it => Block.UNKNOWN).ToArray());

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "-", "o", "-", "-", "-");
        }

        [TestMethod]
        public void LengthGreaterThanRuleSumAndMinCrossedSum_CommonResults1()
        {
            var lineInfo = new LineInfo(new int[] { 7, 1,1,1 }, 3, 10, 15, Enumerable.Range(0, 15).Select(it => Block.UNKNOWN).ToArray());

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "-", "-", "o", "o", "o", "o", "o", "-", "-", "-", "-", "-", "-", "-", "-");
        }

        [TestMethod]
        public void LengthGreaterThanRuleSumAndMinCrossedSum_NoCommonResult()
        {
            var lineInfo = new LineInfo(new int[] { 2, 2, 1, 1, 2 }, 4, 8, 15, Enumerable.Range(0, 15).Select(it => Block.UNKNOWN).ToArray());

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, Enumerable.Range(0, 15).Select(it => "-").ToArray());
        }

        [TestMethod]
        public void LengthGreaterThanRuleSumAndMinCrossedSum_NoCommonResult2()
        {
            var lineInfo = new LineInfo(new int[] { 1,4,2,3 }, 3, 10, 20, 
                
                new Block[] 
                {
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.FILLED,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.FILLED,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.CROSSED,
                    Block.FILLED,
                    Block.FILLED,
                    Block.FILLED,
                    Block.CROSSED
                });

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, Enumerable.Range(0, 20).Select(it => "-").ToArray());
        }

        [TestMethod]
        public void LengthGreaterThanRuleSumAndMinCrossedSum_NoCommonResult3()
        {
            var lineInfo = new LineInfo(new int[] { 1, 6 }, 1, 7, 20,

                new Block[]
                {
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.CROSSED,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN,
                    Block.UNKNOWN
                });

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, Enumerable.Range(0, 20).Select(it => "-").ToArray());
        }


        [TestMethod]
        public void OneRuleForZeroFilledBlocks()
        {
            var lineInfo = new LineInfo(new int[] { 0 }, 0, 0, 3);

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "x", "x", "x");
        }

        [TestMethod]
        public void OneRuleSameAsLineLength()
        {
            var lineInfo = new LineInfo(new int[1] { 3 }, 0, 3, 3);

            var actual = this._sut.FindBlocks(lineInfo);

            AssertLine(actual, "o", "o", "o");
        }


        private void AssertLine(Block[] actual, params string[] expected)
        {
            Assert.IsNotNull(actual, "Actual obj is null.");
            Assert.IsTrue(actual.Length == expected.Length, "Actual obj is not as same length as the Expected obj.");

            for(var index = 0; index < expected.Length; index++)
            {
                Assert.AreEqual(actual[index].Symbol, expected[index]);
            }
        }
    }
}
