﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class BoardInfo
    {
        public BoardSideInfo Horizontal { get; set; }
        public BoardSideInfo Vertical { get; set; }
    }

    public class BoardSideInfo
    {
        public int Length { get; set; }

        public int[][] Rules { get; set; }
    }
}
