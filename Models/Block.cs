﻿namespace Models
{
    public sealed class Block
    {

        public string Name { get; }
        public string Symbol { get; }
        public int Id { get; }

        public static readonly Block UNKNOWN = new Block(0, "Unknown", Symbols.Unknown.ToString());
        public static readonly Block CROSSED = new Block(100, "Crossed", Symbols.Crossed.ToString());
        public static readonly Block FILLED = new Block(200, "Filled", Symbols.Filled.ToString());

        private Block(int id, string name, string symbol)
        {
            this.Id = id;
            this.Name = name;
            this.Symbol = symbol;
        }

        public override string ToString()
        {
            return Name;
        }


        public sealed class Symbols
        {
            private string Symbol { get; }

            public static readonly Symbols Unknown = new Symbols("-");
            public static readonly Symbols Crossed = new Symbols("x");
            public static readonly Symbols Filled = new Symbols("o");

            private Symbols(string symbol)
            {
                this.Symbol = symbol;
            }

            public override string ToString()
            {
                return Symbol;
            }
        }
    }

    
}
