﻿namespace Models
{
    public class LineInfo
    {
        public LineInfo(int[] rules, int minCrossedBlocks, int ruleSum, int length, params Block[] blocks)
        {
            this.Rules = rules;
            this.Length = length;
            this.Blocks = blocks;
            this.MinRequredCrossedSum = minCrossedBlocks;
            this.RuleSum = ruleSum;
        }


        public int[] Rules { get; }

        public int RuleSum { get; }

        public int MinRequredCrossedSum { get; }

        public int Length { get;  }

        public Block[] Blocks { get; }
    }
}
