﻿namespace Services
{
    using Models;
    using Interfaces;
    using System.Linq;
    using System.Collections.Generic;

    public class MissingBlockFinder : IMissingBlockFinder
    {
        public Block[] FindBlocks(LineInfo line)
        {
            if (line.RuleSum == 0)
            {
                return Enumerable.Range(0, line.Length)
                       .Select(it => Block.CROSSED)
                       .ToArray();
            }

            if (line.RuleSum == line.Length)
            {
                return Enumerable.Range(0, line.Length)
                        .Select(it => Block.FILLED)
                        .ToArray();
            }

            if (line.RuleSum + line.MinRequredCrossedSum == line.Length)
            {
                var lineBlocks = Enumerable.Range(0, line.Length)
                        .Select(it => Block.FILLED)
                        .ToArray();

                var relativeChainIndex = 0;

                for (int ruleIndex = 0; ruleIndex < line.Rules.Length; ruleIndex++)
                {
                    var chainLength = line.Rules[ruleIndex];
                    if (ruleIndex + 1 != line.Rules.Length)
                    {
                        relativeChainIndex += chainLength;
                        var indexToCross = relativeChainIndex;

                        if (indexToCross < lineBlocks.Length)
                        {
                            lineBlocks[indexToCross] = Block.CROSSED;
                        }

                        relativeChainIndex++;
                    }

                }

                return lineBlocks;
            }
            else
            {
                var knownLengthDifference = 1 + (line.Length - (line.RuleSum + line.MinRequredCrossedSum));

                var lineResult = new Block[line.Length];
                var attemptedLines = new Block[knownLengthDifference][];

                for (int attemptIndex = 0; attemptIndex < knownLengthDifference; attemptIndex++)
                {
                    var lineBlocks = Enumerable.Range(0, line.Length)
                     .Select(it => Block.FILLED)
                     .ToArray();

                    var indexToCross = attemptIndex;

                    for (int index = 0; index < attemptIndex; index++)
                    {
                        lineBlocks[index] = Block.CROSSED;
                    }

                    for (int ruleIndex = 0; ruleIndex < line.Rules.Length; ruleIndex++)
                    {
                        var chainLength = line.Rules[ruleIndex];
                        indexToCross += chainLength;

                        if (indexToCross < lineBlocks.Length)
                        {
                            lineBlocks[indexToCross] = Block.CROSSED;
                            indexToCross++;
                        }

                    }


                    for (int index = indexToCross; index < lineBlocks.Length; index++)
                    {
                        lineBlocks[index] = Block.CROSSED;
                    }


                    attemptedLines[attemptIndex] = lineBlocks;
                }

                var filteredAttemptedLines = FilterGuessLines(attemptedLines, line).ToList();

                if (filteredAttemptedLines.Any())
                {
                    var result = filteredAttemptedLines.FirstOrDefault();

                    for (int compIndex = 1; compIndex < filteredAttemptedLines.Count; compIndex++)
                    {
                        result = MergeAttempts(result, filteredAttemptedLines.ElementAt(compIndex));
                    }

                    return result;
                }

                return Enumerable.Range(0, line.Length)
                   .Select(it => Block.UNKNOWN)
                   .ToArray();
            }
        }

        private List<Block[]> FilterGuessLines(Block[][] attemptedLines, LineInfo lineInfo)
        {
            var result = new List<Block[]>();

            for (int lineIndex = 0; lineIndex < attemptedLines.Length; lineIndex++)
            {
                var testLine = attemptedLines[lineIndex];

                if (IsValidLine(testLine, lineInfo))
                {
                    result.Add(testLine);
                }
            }

            return result;
        }

        private bool IsValidLine(Block[] testLine, LineInfo lineInfo)
        {
            for (int index = 0; index < lineInfo.Length; index++)
            {
                if (lineInfo.Blocks[index] == Block.UNKNOWN)
                {
                    continue;
                }

                if (lineInfo.Blocks[index] != testLine[index])
                {
                    return false;
                }
            }

            return true;
        }

        private Block[] MergeAttempts(Block[] a, Block[] b)
        {
            var merged = new Block[a.Length];

            for (int index = 0; index < a.Length; index++)
            {
                if (a[index] == b[index] && a[index] == Block.FILLED)
                {
                    merged[index] = Block.FILLED;
                }
                else
                {
                    merged[index] = Block.UNKNOWN;
                }
            }

            return merged;
        }
    }
}
