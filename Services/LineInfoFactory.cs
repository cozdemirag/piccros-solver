﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class LineInfoFactory
    {
        public LineInfo Create(int[] rules, int length, params Block[] squares)
        {
            if(length < 1)
            {
                throw new Exception($"Invalid Line length - {length}");
            }

            var ruleValidationResult = this.GetValidatedRules(rules, length);

            var validatedSquares = squares.Take(length).ToArray();

            return new LineInfo(ruleValidationResult.Rules, ruleValidationResult.MinRequiredCrossedBlocks, ruleValidationResult.RulesSum, length, validatedSquares);
        }

        private RulesValidationResult GetValidatedRules(int[] inputRules, int length)
        {
            if(inputRules == null && inputRules.Length == 0)
            {
                return new RulesValidationResult(new int[1] { 0 }, 0, 0);
            }

            var minCrossedBlocks = inputRules.Length - 1;
            var sumOfFilledBlocks = inputRules.Sum();

            if(minCrossedBlocks + sumOfFilledBlocks > length)
            {
                throw new ArgumentOutOfRangeException(nameof(inputRules), $"Invalid Rules found for line with length - {length}");
            }

            return new RulesValidationResult(inputRules, minCrossedBlocks, sumOfFilledBlocks);
        }


        private class RulesValidationResult
        {
            public RulesValidationResult(int[] rules, int minCrossedBlocks, int rulesSum)
            {
                this.Rules = rules;
                this.MinRequiredCrossedBlocks = minCrossedBlocks;
                this.RulesSum = rulesSum;
            }

            public int[] Rules { get; }

            public int MinRequiredCrossedBlocks { get; }

            public int RulesSum { get; }
        }
    }
}
