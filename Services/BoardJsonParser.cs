﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Services
{
    public class BoardJsonParser
    {
        public BoardInfo Parse(string filePath)
        {
            var contents = File.ReadAllText(filePath);

            return JsonConvert.DeserializeObject<BoardInfo>(contents);
        }
    }
}
