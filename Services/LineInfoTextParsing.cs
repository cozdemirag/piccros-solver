﻿using Models;
using System;
using System.Linq;

namespace Services
{
    public class LineInfoTextParsing
    {
        private static readonly string[] AllowedBlockStrings = new string[2] { "x", "o" };
        private readonly LineInfoFactory factory;

        public LineInfoTextParsing()
        {
            this.factory = new LineInfoFactory();
        }

        public LineInfo Parse(string[] args)
        {
            if (args == null || args.Length < 3)
            {
                throw new Exception($"Missing parameters");
            }

            var length = 0;
            if (!int.TryParse(args[1], out length))
            {
                throw new Exception($"Invalid length parameter value - {length}");
            }

            var rulesString = args[2];
            if (string.IsNullOrWhiteSpace(rulesString))
            {
                rulesString = "0";
            }

            var rules = rulesString.Split(',').Select(it => int.Parse(it)).ToArray();

            var blocks = Enumerable.Range(0, length).Select(it => Block.UNKNOWN).ToArray();
            if(args.Length >= 4)
            {
                var blocksString = args[3];

                if (string.IsNullOrWhiteSpace(blocksString))
                {
                    blocksString = string.Empty;
                }

                blocks = blocksString.Split(',').Select(MapToBlock).ToArray();
            }

            return this.factory.Create(rules, length, blocks);
        }

        private Block MapToBlock(string inputStr)
        {
            var input = inputStr?.ToLower();

            if(string.IsNullOrWhiteSpace(input) || !AllowedBlockStrings.Contains(input))
            {
                return Block.UNKNOWN;
            }

            if (input.Equals(Block.Symbols.Crossed.ToString()))
            {
                return Block.CROSSED;
            }

            if (input.Equals(Block.Symbols.Filled.ToString()))
            {
                return Block.FILLED;
            }

            throw new Exception("Unknown block type found");
        }
    }
}
