﻿namespace Interfaces
{
    using Models;

    public interface IMissingBlockFinder
    {
        Block[] FindBlocks(LineInfo line);
    }
}
